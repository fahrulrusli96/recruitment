<?php
class Loker_model extends CI_Model{

	function loker_list(){
		$hasil=$this->db->get('ms_loker');
		return $hasil->result();
	}

	function save_loker(){
		$data = array(
				'posisi_loker' 	=> $this->input->post('posisi_loker'), 
				'detail_loker' 	=> $this->input->post('detail_loker'), 
				'created_at' 	=> date('Y-m-d'),
				'expired_date'	=> $this->input->post('expired_date'),
				'status_loker'	=> $this->input->post('status_loker'),

			);
		$result=$this->db->insert('ms_loker',$data);
		return $result;
	}

	function update_loker(){
		$id=$this->input->post('id');
		$posisi_loker=$this->input->post('posisi_loker');
		$detail_loker=$this->input->post('detail_loker');
		$expired_date=$this->input->post('expired_date');
		$status_loker=$this->input->post('status_loker');

		$this->db->set('posisi_loker', $posisi_loker);
		$this->db->set('detail_loker', $detail_loker);
		$this->db->set('expired_date', $expired_date);
		$this->db->set('status_loker', $status_loker);

		$this->db->where('id', $id);
		$result=$this->db->update('ms_loker');
		return $result;
	}

	function delete_loker(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('ms_loker');
		return $result;
	}
	
}