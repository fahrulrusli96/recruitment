<?php
class Pelamar_model extends CI_Model{

	function pelamar_list(){
		$this->db->select('tr_lamaran.nama_pelamar, tr_lamaran.no_hp, tr_lamaran.email, tr_lamaran.file_cv, tr_lamaran.file_foto, tr_lamaran.date_apply, tr_lamaran.status_apply, ms_loker.posisi_loker, ms_loker.detail_loker')
         ->from('tr_lamaran')
         ->join('ms_loker', 'ms_loker.id = tr_lamaran.id_loker');
        $hasil = $this->db->get();
		return $hasil->result();
	}

	function save_pelamar(){
		$data = array(
				'id_loker' 	=> $this->input->post('id_loker'), 
				'nama_pelamar' 	=> $this->input->post('nama_pelamar'), 
				'no_hp' 	=> $this->input->post('no_hp'), 
				'email'	=> $this->input->post('email'),
				'date_apply'	=> date('Y-m-d'),
				'status_apply'	=> 0,


			);
		$result=$this->db->insert('tr_lamaran',$data);
		return $result;
	}

	function update_pelamar(){
		$id=$this->input->post('id');
		$id_loker=$this->input->post('status_apply');

		$this->db->set('status_apply', $status_apply);
		
		$this->db->where('id', $id);
		$result=$this->db->update('tr_lamaran');
		return $result;
	}

	function delete_pelamar(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('tr_lamaran');
		return $result;
	}
	
}