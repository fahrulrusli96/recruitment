<?php
class Loker extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('loker_model');

	}
	function index(){
		$this->load->model("auth/user_model");
		if($this->user_model->isNotLogin()) redirect(site_url('auth/login'));
		$this->template->load('template', 'admin/loker_view');
	}

	function loker_data(){
		$data=$this->loker_model->loker_list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->loker_model->save_loker();
		echo json_encode($data);
	}

	function update(){
		$data=$this->loker_model->update_loker();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->loker_model->delete_loker();
		echo json_encode($data);
	}

}