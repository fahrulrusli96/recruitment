<?php
class Pelamar extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('pelamar_model');

	}
	function index(){
		$this->load->model("auth/user_model");
		if($this->user_model->isNotLogin()) redirect(site_url('auth/login'));
		$this->template->load('template', 'admin/pelamar_view');
	}

	function pelamar_data(){
		$data=$this->pelamar_model->pelamar_list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->pelamar_model->save_pelamar();
		echo json_encode($data);
	}

	function update(){
		$data=$this->pelamar_model->update_pelamar();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->pelamar_model->delete_pelamar();
		echo json_encode($data);
	}

}