<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model("auth/user_model");
		if($this->user_model->isNotLogin()) redirect(site_url('auth/login'));
	}
	function index(){
		$this->template->load('template', 'admin/dashboard');
	}


}