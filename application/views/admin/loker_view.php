 <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Tables</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Tables</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tables</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                  <div class="float-right"><a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"><span class="fa fa-plus"></span> Add New</a></div>
            </div>

            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush" id="mydata">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Posisi Loker</th>
                    <th scope="col" class="sort" data-sort="budget">Tanggal Expired</th>
                    <th scope="col" class="sort" data-sort="status">Status Loker</th>
                    <th scope="col" class="sort" data-sort="completion">Actions</th>
                  </tr>
                </thead>
                <tbody id="show_data">
                 
                </tbody>
              </table>
            </div>
         
          </div>
        </div>
      </div>
 <!-- <div class="container-fluid mt--6">
    <div class="row">
        <div class="card bg-default">
            <div class="col-12">
                <div class="col-md-12">
                    <h1>loker
                        <small>List</small>
                        <div class="float-right"><a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"><span class="fa fa-plus"></span> Add New</a></div>
                    </h1>
                </div>

                <table class="table table-striped" id="mydata">
                    <thead>
                        <tr>
                            <th>loker Code</th>
                            <th>loker Name</th>
                            <th>Price</th>
                            <th style="text-align: right;">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="show_data">

                    </tbody>
                </table>
            </div>
        </div>
    </div> -->
        

		<!-- MODAL ADD -->
            <form>
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New loker</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Posisi Loker</label>
                            <div class="col-md-10">
                              <input type="text" name="posisi_loker" id="posisi_loker" class="form-control" placeholder="Nama Loker">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Detail Loker</label>
                            <div class="col-md-10">
                              <textarea id="detail_loker" name="detail_loker" style="height: 500px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Tanggal Expired</label>
                            <div class="col-md-10">
                              <input type="text" name="expired_date" id="expired_date" class="form-control" placeholder="Expired Date">
                            </div>
                        </div>
                           <div class="form-group row">
                            <label class="col-md-2 col-form-label">Status Loker</label>
                            <div class="col-md-10">
                              <select name="status_loker" id="status_loker" class="form-control">
                                <option value="0">Close</option>
                                <option value="1">Open</option>
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL ADD-->

        <!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit loker</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                         
                        <input type="hidden" name="id_edit" id="id_edit" class="form-control" placeholder="loker Code">
                     
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Posisi Loker</label>
                            <div class="col-md-10">
                              <input type="text" name="posisi_loker_edit" id="posisi_loker_edit" class="form-control" placeholder="Nama Loker">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Detail Loker</label>
                            <div class="col-md-10">
                              <textarea id="detail_loker_edit" name="detail_loker_edit" style="height: 500px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Tanggal Expired</label>
                            <div class="col-md-10">
                              <input type="text" name="expired_date_edit" id="expired_date_edit" class="form-control" placeholder="Expired Date">
                            </div>
                        </div>
                           <div class="form-group row">
                            <label class="col-md-2 col-form-label">Status Loker</label>
                            <div class="col-md-10">
                              <select name="status_loker_edit" id="status_loker_edit" class="form-control">
                                <option value="0">Close</option>
                                <option value="1">Open</option>
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->

        <!--MODAL DELETE-->
         <form>
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete loker</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete this record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="id_delete" id="id_delete" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->

</div>

<script type="text/javascript">
	$(document).ready(function(){

    var ckeditor = CKEDITOR.replace('detail_loker',{
      height:'400px'
    });
    var ckeditor = CKEDITOR.replace('detail_loker_edit',{
      height:'400px'
    });

    CKEDITOR.disableAutoInline = true;
    CKEDITOR.inline('editable');


		show_loker();	//call function show all loker
		
		$('#mydata').dataTable();
		 
	
        //Save loker
        $('#btn_save').on('click',function(){
            var id = $('#id').val();
            var posisi_loker = $('#posisi_loker').val();
            var detail_loker = CKEDITOR.instances['detail_loker'].getData();
            var expired_date = $('#expired_date').val();
            var status_loker = $('#status_loker').val();
            // console.log(detail_loker);
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('loker/save')?>",
                dataType : "JSON",
                data : {posisi_loker:posisi_loker, detail_loker:detail_loker, expired_date:expired_date, status_loker:status_loker},
                success: function(data){
                    show_loker();
            
                    $('[name="posisi_loker"]').val("");
                    CKEDITOR.instances['detail_loker'].setData('');
                    $('[name="expired_date"]').val("");
                    $('[name="status_loker"]').val("");
                    $('#Modal_Add').modal('hide');
                    location.reload();
                    
                }
            });
            return false;
        });

        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
            var id = $(this).data('id');
            var posisi_loker = $(this).data('posisi_loker');
            var detail_loker        = $(this).data('detail_loker');
            var expired_date        = $(this).data('expired_date');
            var status_loker        = $(this).data('status_loker');
            
            $('#Modal_Edit').modal('show');
            $('[name="id_edit"]').val(id);
            $('[name="posisi_loker_edit"]').val(posisi_loker);
            CKEDITOR.instances['detail_loker_edit'].setData(detail_loker);
            $('[name="expired_date_edit"]').val(expired_date);
            $('[name="status_loker_edit"]').val(status_loker);


        });

        //update record to database
         $('#btn_update').on('click',function(){
            var id = $('#id_edit').val();
            var posisi_loker = $('#posisi_loker_edit').val();
            var detail_loker = CKEDITOR.instances['detail_loker_edit'].getData();
            var expired_date = $('#expired_date_edit').val();
            var status_loker = $('#status_loker_edit').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('loker/update')?>",
                dataType : "JSON",
                data : {id:id,posisi_loker:posisi_loker, detail_loker:detail_loker, expired_date:expired_date, status_loker:status_loker},
                success: function(data){
                    $('[name="id_edit"]').val("");
                    $('[name="posisi_loker_edit"]').val("");
                    CKEDITOR.instances['detail_loker'].setData('');
                    $('[name="expired_date_edit"]').val("");
                    $('[name="status_loker_edit"]').val("");
                    $('#Modal_Edit').modal('hide');
                    show_loker();
                    location.reload();
                }
            });
            return false;
        });

        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
            var id = $(this).data('id');
            
            $('#Modal_Delete').modal('show');
            $('[name="id_delete"]').val(id);
        });

        //delete record to database
         $('#btn_delete').on('click',function(){
            var id = $('#id_delete').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('loker/delete')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="id_delete"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_loker();
                }
            });
            return false;
        });

	});

        //function show all loker
        function show_loker(){
            $.ajax({
                type  : 'ajax',
                url   : '<?php echo site_url('loker/loker_data')?>',
                async : false,
                dataType : 'json',
                success : function(data){
                  console.log(data);
                    var html = '';
                    var i;
                    var status;
                    for(i=0; i<data.length; i++){
                      if (data[i].status_loker == 0)
                        status = 'Close';
                      else
                        status = 'Open';

                        html += '<tr>'+
                                '<td>'+data[i].posisi_loker+'</td>'+
                                '<td>'+data[i].expired_date+'</td>'+
                                 '<td>'+status+'</td>'+
                                '<td style="text-align:right;">'+
                                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-posisi_loker="'+data[i].posisi_loker+'" data-detail_loker="'+data[i].detail_loker+'" data-expired_date="'+data[i].expired_date+'" data-status_loker="'+data[i].status_loker+'">Edit</a>'+' '+
                                    '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Delete</a>'+
                                '</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }

            });
        }


</script>