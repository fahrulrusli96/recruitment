  <!-- Topnav -->
   
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Default</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                      <span class="h2 font-weight-bold mb-0">2,356</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="ni ni-chart-pie-35"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                      <span class="h2 font-weight-bold mb-0">924</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                      <span class="h2 font-weight-bold mb-0">49,65%</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
     
      <div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Informasi Lowongan Kerja</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush" id="mydata">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">SUBJECT LOKER</th>
                    <th scope="col" class="sort" data-sort="budget">TANGGAL EXPIRED</th>
                    <th scope="col" class="sort" data-sort="status">STATUS LOKER</th>
                    <th scope="col" class="sort" data-sort="completion">ACTIONS</th>
                  </tr>
                </thead>
                <tbody id="show_data">
                 
                </tbody>
              </table>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


    <form>
      <div class="modal fade" id="Modal_Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Detail Loker : <span id="loker"></span></h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>

            </div>
            <div class="modal-body">
              <span id="isi"></span>
             
           </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <button type="button" type="submit" id="btn_apply" class="btn btn-primary">APPLY</button>
          </div>
        </div>
      </div>
    </div>
  </form>


      <div class="modal fade" id="Modal_Apply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Masukkan Data Anda Untuk Mengirim Lamaran</h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>

            </div>
            <div class="modal-body">

             <form enctype="multipart/form-data" id="submit">
                <input type="hidden" name="id" id="id" class="form-control">

                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Nama</label>
                  <div class="col-md-9">
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Anda" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Nomor HP</label>
                  <div class="col-md-9">
                    <input type="text" name="no_hp" id="no_hp" class="form-control" placeholder="Nomor HP" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Email</label>
                  <div class="col-md-9">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">UPLOAD YOUR CV</label>
                  <div class="col-md-9">
                    <input name="file" type="file"  id="cv_file" required>
                    
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">UPLOAD YOUR FOTO</label>
                  <div class="col-md-9">
                   
                     <input name="file2" type="file"  id="foto_file" required>
                    
                  </div>
                </div>
             
           </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <button type="submit" class="btn btn-primary">SEND DATA</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">

        $('#submit').submit(function(e){
    
          e.preventDefault(); 
          $.ajax({
             url:'<?php echo site_url('home/do_upload')?>',
             type:"post",
             data:new FormData(this),
             processData:false,
             contentType:false,
             cache:false,
             async:false,
             success: function(data){
              alert(data);
            }
          });
        }); 
        show_pelamar();
        $('#mydata').dataTable();
        $('#show_data').on('click','.item_detail',function(){
            var id = $(this).data('id');
            var isi = $(this).data('detail');
            var loker = $(this).data('posisi_loker');
            $('#Modal_Detail').modal('show');
            $('[name="id"]').val(id);
            $('#isi').html(isi);
            $('#loker').html(loker);


        });


       function show_pelamar(){
            $.ajax({
                type  : 'ajax',
                url   : '<?php echo site_url('loker/loker_data')?>',
                async : false,
                dataType : 'json',
                success : function(data){
                  console.log(data);
                    var html = '';
                    var i;
                    var status;
                    for(i=0; i<data.length; i++){
                      if (data[i].status_loker == 0)
                        status = 'Close';
                      else
                        status = 'Open';

                        html += '<tr>'+
                                '<td>'+data[i].posisi_loker+'</td>'+
                                '<td>'+data[i].expired_date+'</td>'+
                                '<td>'+status+'</td>'+
                                '<td><a href="javascript:void(0);" class="btn-icon-clipboard item_detail" data-id="'+data[i].id+'" data-detail="'+data[i].detail_loker+'" data-posisi_loker="'+data[i].posisi_loker+'" ><center><i class="ni ni-box-2"></i><span>Lihat Detail</span></center></a></td>'+
                                ''+
                                
                              
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }

            });
        }

        $("#btn_apply").click(function(){
            $('#Modal_Apply').modal('show');
        });
 

    </script>