/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : recruitment

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 26/02/2021 16:43:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ms_loker
-- ----------------------------
DROP TABLE IF EXISTS `ms_loker`;
CREATE TABLE `ms_loker`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posisi_loker` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detail_loker` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` date NULL DEFAULT NULL,
  `expired_date` date NULL DEFAULT NULL,
  `status_loker` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ms_loker
-- ----------------------------
INSERT INTO `ms_loker` VALUES (1, 'IT PROGRAMMER', 'HTML, CSS, JAVASCRIPT, JAVA, FRAMEWORK', '2021-02-26', '2021-03-08', 1);
INSERT INTO `ms_loker` VALUES (3, 'Sekretaris', '<p>zxcxvxcvxcv</p>\n\n<ol>\n	<li>asdasdsadasdasd</li>\n	<li>asdasdasd</li>\n</ol>\n', '2021-02-26', '2020-03-28', 1);
INSERT INTO `ms_loker` VALUES (6, 'UHUY', '<h1>asdasdasd :</h1>\n\n<ul>\n	<li>asdasdasd</li>\n	<li>asdasdasd</li>\n	<li>asddsad</li>\n</ul>\n', '2021-02-26', '2021-03-28', 1);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_code` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `product_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `product_price` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`product_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('09', 'dfgdf', 890);
INSERT INTO `product` VALUES ('123', 'test', 500);
INSERT INTO `product` VALUES ('19739140', 'Product 1', 30);
INSERT INTO `product` VALUES ('19739141', 'Product 2', 13);
INSERT INTO `product` VALUES ('19739142', 'Product 3', 34);
INSERT INTO `product` VALUES ('19739143', 'Product 4', 65);
INSERT INTO `product` VALUES ('19739144', 'Product 5', 32);
INSERT INTO `product` VALUES ('19739145', 'Product 6', 76);
INSERT INTO `product` VALUES ('19739146', 'Product 7', 90);
INSERT INTO `product` VALUES ('55', 'ff', 55);
INSERT INTO `product` VALUES ('5665', 'gdfgfd', 345646);
INSERT INTO `product` VALUES ('567', 'dfg', 657);
INSERT INTO `product` VALUES ('767', 'asd', 456);
INSERT INTO `product` VALUES ('87', 'sdf', 678);
INSERT INTO `product` VALUES ('88', 'jj', 88);
INSERT INTO `product` VALUES ('90', 'hsh', 90);

-- ----------------------------
-- Table structure for tr_lamaran
-- ----------------------------
DROP TABLE IF EXISTS `tr_lamaran`;
CREATE TABLE `tr_lamaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_loker` int(11) NULL DEFAULT NULL,
  `nama_pelamar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_apply` date NULL DEFAULT NULL,
  `status_apply` int(11) NULL DEFAULT NULL,
  `file_cv` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `file_foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_lamaran
-- ----------------------------
INSERT INTO `tr_lamaran` VALUES (1, 1, 'Fachrul', '081212813679', 'fahrulrusli96@gmail.com', '2021-02-26', 0, 'asdasd.pdf', 'asdasd.jpg');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_access` int(11) NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `is_active` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'superadmin', '$2y$10$Dy4ySqOiDqKisEyoKJEsl.L2psUIjeKMwmp5W3iElRQLKZF0.kKbS', 1, '2021-02-26 15:07:17', 1);

SET FOREIGN_KEY_CHECKS = 1;
